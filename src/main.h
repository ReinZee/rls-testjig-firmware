#pragma once
#include <stdint.h>

//evabus_host_header_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
    uint8_t address;
    uint8_t length;
    uint8_t type;
  };
  uint8_t data[301];
} evabus_host_header_t;

//evabus_host_measurement_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
    uint8_t address;
    uint8_t length;
    uint8_t type;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t unused;
    uint8_t status;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_measurement_packet_t;

//evabus_host_id_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_id_packet_t;

//evabus_host_cmd_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint8_t target_12V;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_cmd_packet_t;

//evabus_host_set_servos_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint8_t servo;
      uint8_t angle;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_set_servos_packet_t;


//evabus_host_write_modbus_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint16_t reg;
      uint16_t value; 
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_write_modbus_packet_t;

//evabus_host_set_serial_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint8_t baud_select;
      uint8_t parity_select;
      uint8_t stopbits;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_set_serial_packet_t;

//evabus_host_set_slave_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint8_t slave_address;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_set_slave_packet_t;

//evabus_host_startup_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      char ok_token[8];
      uint8_t num_ok;
      char success_token[8];
      uint8_t timeout;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_startup_packet_t;

//evabus_host_read_modbus_packet_t
typedef union {
  struct __attribute__ ((packed)) {
    uint16_t ID;
      uint8_t address;
      uint8_t length;
      uint8_t type;
      uint8_t on_off_single;
    uint16_t crc;
  };
  uint8_t data[301];
} evabus_host_read_modbus_packet_t;

//evabus_client_measurement_packet_t
typedef union {
  struct __attribute__ ((packed)){
    uint16_t ID;
    uint8_t address;
    uint8_t length;
    uint8_t packet_type;
    uint8_t type;
    uint8_t button_state;
    uint8_t target_12V;           //8
    float MB_12V;
    float MB_GND;
    float MB_A;
    float MB_B;
    float PWR_5V0;
    float PWR_3V3;
    float PWR_1V8;
    float PWR_GND;                //40
    float MA;                     //44
    uint8_t startup_success;
    uint8_t startup_done;
    uint8_t startup_num_ok;
    uint16_t debug_distance;      //49
    uint16_t debug_amplitude;
    uint16_t modbus_distance;  //DISTANCE 1 AMPLITUDE 2 VERSION 3 ACC_VERSION 4
    uint16_t modbus_amplitude;
    uint16_t modbus_version;
    uint16_t modbus_acc_version;  //59
    uint16_t radar_serial_number[2];
    uint16_t debug_slave_id;
    uint16_t debug_baud_select;
    uint8_t  modbus_success_ctr;
    uint8_t  modbus_fail_ctr;
    uint16_t current_modbus_baud;
    uint8_t  current_modbus_slave_id; //72
    uint8_t debug_stopbits;
    uint8_t debug_parity_select;
    uint8_t current_modbus_stopbits;
    uint8_t current_modbus_parity_select;
    uint16_t crc;                 //78
  };
  uint8_t data[61];
} evabus_client_measurement_packet_t;

//evabus_client_id_packet_t
typedef union {
  struct __attribute__ ((packed)){
    uint16_t ID;
    uint8_t address;
    uint8_t length;
    uint8_t packet_type;
    char firmware_version[8];
    uint8_t uuid[6];
    uint16_t crc;
  };
  uint8_t data[21];
} evabus_client_id_packet_t;

//evabus_client_bootloader_packet_t
typedef union {
  struct __attribute__ ((packed)){
    uint16_t ID;
    uint8_t address;
    uint8_t length;
    uint8_t packet_type;
    uint16_t crc;
  };
  uint8_t data[7];
} evabus_client_bootloader_packet_t;





