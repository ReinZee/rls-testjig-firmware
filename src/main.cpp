// Radar: Niqo-systems
// Version: 1.0.3
// Latest Revision Date: 27-07-2020
// By EVAbits B.V.
// Author: Jan Stegenga

#include <Arduino.h>
#include <Bounce2.h>      //debounce the button
#include "main.h"         //message formats
#include <ModbusMaster.h> //modbus master
#include <watchdog.h>     //16s watchdog
#include <MyServo.h>


// pin definitions in variant.h
// #define BUTTON_PIN                    15 //PB08 pin7   
// #define LED_RED                       30 //PB11 pin20    
// #define LED_GREEN                     29 //PB10 pin19    
// #define LED_BLUE                      16 //PB09 pin8   
// #define PIN_12V                       37 //PB22 pin37
// #define MB_GND_CHECK                  14 //PA02 pin3
// #define PWR_5V_CHECK                  17 //PA04 pin9
// #define PWR_3V3_CHECK                 18 //PA05 pin10
// #define GND_TARGET                    19 //PA06 pin11
// #define MB_A_CHECK                    20 //PA07 pin12
// #define MA_CHECK                      21 //PB02 pin47   //mAchckP               
// #define MB_12V_CHECK                  36 //PB03 pin48   
// #define MB_B_CHECK                    24 //PA08 pin13   
// #define PWR_1V8_CHECK                 25 //PA09 pin14
// #define SERVO1                        27 //PA12 pin21
// #define SERVO2                        28 //PA13 pin22


// evabus
#define EVABUS_ID_TYPE                0x00
#define EVABUS_MEASUREMENT_TYPE       0x01
#define EVABUS_READ_MODBUS_TYPE       0x03
#define EVABUS_CMD_TYPE               0x04
#define EVABUS_STARTUP_TYPE           0x05
#define EVABUS_SET_SERIAL_TYPE        0x06
#define EVABUS_SLAVE_TYPE             0x07
#define EVABUS_SET_SERVOS_TYPE        0x08
#define EVABUS_SLAVE_ID               0x09 //JIG SLAVE ID
#define EVABUS_WRITE_MODBUS_TYPE      0x0A
#define EVABUS_JIG_TYPE               0x00 //unique sensor type definition
#define EVABUS_ENTER_BOOTLOADER_TYPE  0xff

// modbus coil addresses
#define MB_RADAR_COIL_ADDR            1

// modbus holding addresses
#define MB_RADAR_RANGE_START          0
#define MB_RADAR_RANGE_END            1
#define MB_RADAR_GAIN                 2
#define MB_RADAR_THRESHOLD            3
#define MB_RADAR_RUNNING_AVG_FACTOR   4 
#define MB_RADAR_CAL_A                5
#define MB_RADAR_CAL_B                6
#define MB_RADAR_CAL_OFFSET           7
#define MB_MODBUS_SLAVE_ID            8
#define MB_MODBUS_BAUD_ID             9  //
#define MB_MODBUS_STOPBITS_ID         10 //
#define MB_MODBUS_PARITY_SELECT_ID    11 //

// modbus input addresses
#define MB_DISTANCE                   1
#define MB_AMPLITUDE                  2
#define MB_VERSION                    3
#define MB_ACC_VERSION                4
#define MB_UUID_H                     5 //version ^110
#define MB_UUID_L                     6 //version ^110

// GIT
#ifndef GIT_VERSION
   #define GIT_VERSION                "1.0.4   "  //7 chars
#endif

//analog reference voltage
#define AREF                          AR_DEFAULT //AR_INTERNAL1V65    //AR_DEFAULT    //AR_INTERNAL1V65
#define AREFVOLT                      3.33 //1.65           //3.33          //Use this in calculations

//4-20mA 
#define AMC1301_GAIN                  8.1   //as per spec
#define AMC1301_SHUNT                 12.3  //measured

//shutdown target voltage whenever
#define GND_PWR_MAX_MA                200.0

//min and max angles
#define MIN_SERVO_ANGLE               39    //degrees. this is approx flush with the top of the lower beams. use tape to create a rest for the target
#define MAX_SERVO_ANGLE               180   //degrees.

uint8_t packet_buffer[301]; // MAX 256 + 5
uint8_t target_powered = 0;
String buffer = "";
String ok_str = "OK", success_str = "SUCCESS";
unsigned long time, stup_timeout, mb_checktime, ma_checktime;
bool debug_on = false;
bool mA_check_on = true; 
bool MB_check_on = false;
bool servos_attached = false;
float voltage1, voltage2, voltage_diff=0.00, mA=-1.00, pwr_gnd_mA=0.00;
Servo servo1, servo2, servo3, servo4, servo5;
uint8_t current_servo_angles[5] = {MIN_SERVO_ANGLE, MIN_SERVO_ANGLE, MIN_SERVO_ANGLE, MIN_SERVO_ANGLE, MIN_SERVO_ANGLE};
ModbusMaster node;
Bounce debouncer = Bounce();


// function prototypes:
void set_servo_angle2( uint8_t servo_id, uint8_t new_angle );
void set_servo_angle( uint8_t new_angle, uint8_t old_angle );
void handle_host_measurement_packet(evabus_host_measurement_packet_t *evabus_host_packet);
void handle_host_id_packet(evabus_host_id_packet_t *evabus_host_packet);
void handle_host_cmd_packet(evabus_host_cmd_packet_t *evabus_host_cmd_packet);
void handle_host_startup_packet(evabus_host_startup_packet_t *evabus_host_startup_packet);
void handle_host_set_serial_packet(evabus_host_set_serial_packet_t *evabus_host_set_serial_packet);
void handle_host_set_slave_packet(evabus_host_set_slave_packet_t *evabus_host_set_slave_packet);
void handle_host_read_modbus_packet(evabus_host_read_modbus_packet_t *evabus_host_read_modbus_packet);
void handle_host_write_modbus_packet(evabus_host_write_modbus_packet_t *evabus_host_write_modbus_packet);
void handle_host_set_servos_packet(evabus_host_set_servos_packet_t *evabus_host_set_servos_packet);
void set_button_color(uint8_t r, uint8_t g, uint8_t b);
void blink(int num, int ms, uint8_t r, uint8_t g, uint8_t b);
void debug_16bit(uint16_t val);
uint16_t calculateCRC(uint8_t *buffer, int length);
uint32_t convert_to_le_float(float val);
void set12Vtarget(bool onoff);
void reset_measurement_values(void);
void Serial5_reconfigure(uint16_t baud_set, uint8_t stopbits, uint8_t par_sel);
void checkDebugMessages( String buf, evabus_client_measurement_packet_t * m_pkt );
unsigned long checkStartupMessages( unsigned long _stup_timeout, String buf, String _ok_str, String _success_str, evabus_client_measurement_packet_t * m_pkt );
unsigned long checkModbusInputRegisters( evabus_client_measurement_packet_t * m_pkt, bool _debug_on );
void checkRadarPresence( unsigned long _stup_timeout, uint8_t _target_powered );

//evabus packets
evabus_host_header_t evabus_host_packet  = {{.ID = 0xCDAB, .address = EVABUS_SLAVE_ID}};
evabus_host_id_packet_t evabus_id_packet = {{.ID = 0xCDAB, .address = EVABUS_SLAVE_ID}};
evabus_client_measurement_packet_t evabus_measurement_client_packet = {{.ID = 0xCDAB,
                                                                        .address = EVABUS_SLAVE_ID,
                                                                        .length = 78,
                                                                        .packet_type = EVABUS_MEASUREMENT_TYPE,
                                                                        .type = EVABUS_JIG_TYPE}};
evabus_client_id_packet_t evabus_client_id_packet = {{.ID = 0xCDAB,
                                                      .address = EVABUS_SLAVE_ID,
                                                      .length = 21,
                                                      .packet_type = EVABUS_ID_TYPE}};
evabus_client_bootloader_packet_t evabus_client_bootloader_packet = {{.ID = 0xCDAB,
                                                                      .address = EVABUS_SLAVE_ID,
                                                                      .length = 7,
                                                                      .packet_type = EVABUS_ENTER_BOOTLOADER_TYPE}};
evabus_client_bootloader_packet_t evabus_client_cmd_packet = {{.ID = 0xCDAB,
                                                               .address = EVABUS_SLAVE_ID,
                                                               .length = 7,
                                                               .packet_type = EVABUS_CMD_TYPE}};
evabus_client_bootloader_packet_t evabus_client_set_serial_packet = {{.ID = 0xCDAB,
                                                                    .address = EVABUS_SLAVE_ID,
                                                                    .length = 7,
                                                                    .packet_type = EVABUS_SET_SERIAL_TYPE}};
evabus_client_bootloader_packet_t evabus_client_set_slave_packet = {{.ID = 0xCDAB,
                                                                     .address = EVABUS_SLAVE_ID,
                                                                     .length = 7,
                                                                     .packet_type = EVABUS_SLAVE_TYPE}};
evabus_client_bootloader_packet_t evabus_client_startup_packet = {{.ID = 0xCDAB,
                                                                   .address = EVABUS_SLAVE_ID,
                                                                   .length = 7,
                                                                   .packet_type = EVABUS_STARTUP_TYPE}};
evabus_client_bootloader_packet_t evabus_client_set_servos_packet = {{.ID = 0xCDAB,
                                                                   .address = EVABUS_SLAVE_ID,
                                                                   .length = 7,
                                                                   .packet_type = EVABUS_SET_SERVOS_TYPE}};
evabus_client_bootloader_packet_t evabus_client_read_modbus_packet = {{.ID = 0xCDAB,
                                                                   .address = EVABUS_SLAVE_ID,
                                                                   .length = 7,
                                                                   .packet_type = EVABUS_READ_MODBUS_TYPE}};
evabus_client_bootloader_packet_t evabus_client_write_modbus_packet = {{.ID = 0xCDAB,
                                                                   .address = EVABUS_SLAVE_ID,
                                                                   .length = 7,
                                                                   .packet_type = EVABUS_WRITE_MODBUS_TYPE}};
evabus_host_startup_packet_t startup_cfg;


/* servo1 -> pos 5
 * servo2 -> pos 2
 * servo3 -> pos 4
 * servo4 -> pos 3
 * servo5 -> pos 1
 */

void setup()
{
  watchdog_init(WDT_CONFIG_PER_4K);
  //                                 PHY pinnum
  Serial1.begin(115200, SERIAL_8N2); //Serial1 -> Sercom0 -> PA10/PA11, pin 15,16 -> PI_Xx     -> Serial to PI
  Serial3.begin(57600);              //Serial3 -> Sercom3 -> PA24/PA25, pin 33,34 -> UART_Xx   -> Serial debug info target
  //Serial4.begin(19200, SERIAL_8E1 );  //Serial4 -> Sercom1 -> PA18/PA19, pin 27,28 -> MB_Xx     -> Target modbus before rs485
  Serial5.begin(19200, SERIAL_8E1); //Serial5 -> Sercom2 -> PA14/PA15, pin 14,15 -> MB_Xx_Mon -> Target modbus after  rs485
  // init pins
  pinMode(LED_RED, OUTPUT);      // LED
  pinMode(LED_GREEN, OUTPUT);    // LED
  pinMode(LED_BLUE, OUTPUT);     // LED
  pinMode(BUTTON_PIN, INPUT);    // Button

  pinMode(PIN_12V_TGT, OUTPUT);  // 12V_to target
  pinMode(EF_12V_TGT, INPUT);    // Error flag on 12V_target switch
  pinMode(PWR_1V8_CHECK, INPUT); // 1V8_CHECK
  pinMode(PWR_5V_CHECK, INPUT);  // 5V0_CHECK
  pinMode(PWR_3V3_CHECK, INPUT); // 3V3_CHECK
  pinMode(GND_TARGET, INPUT);    // GND_TARGET

  pinMode(MB_12V_CHECK, INPUT);  // MB_12V_CHECK
  pinMode(MB_B_CHECK, INPUT);    // MB_B_CHECK
  pinMode(MB_A_CHECK, INPUT);    // MB_A_CHECK
  pinMode(MB_GND_CHECK, INPUT);  // MB_GND_CHECK        //MB_12V

  pinMode(PIN_12V_MA, OUTPUT);   // 12V_to 4-20mA readout section
  pinMode(EF_12V_MA, INPUT);     // Error flag on 12V_MA switch
  pinMode(MA_CHECK_N, INPUT);    // 4-20 mA_CHECK
  pinMode(MA_CHECK_P, INPUT);    // 4-20 mA_CHECK
  delay(100);

  // After setting up the button, setup the Bounce instance :
  debouncer.attach(BUTTON_PIN, INPUT); //INPUT_PULLUP);
  debouncer.interval(25);              // interval in ms

  //set firmware version
  strncpy(&evabus_client_id_packet.firmware_version[0], GIT_VERSION, 8);

  // signal start to user + 12V on/off
  set_button_color(64, 64, 64);
  watchdog_reset();

  set12Vtarget(0);
  delay(500);
  watchdog_reset();
  set_button_color(4, 0, 0);

  //analog reference voltage
  analogReference(AREF);

  //setup the startup tokens with default values
  strncpy(&startup_cfg.ok_token[0], "Start  ", 8);      //7 usrchars plus NULL string termination char
  strncpy(&startup_cfg.success_token[0], "FAILED ", 8); //
  startup_cfg.num_ok = 5;
  startup_cfg.timeout = 33;

  // start modbus master
  node.begin(1, Serial5);
  //set modbus polling info
  evabus_measurement_client_packet.current_modbus_slave_id = 1;
  evabus_measurement_client_packet.current_modbus_baud = 19200;      //
  evabus_measurement_client_packet.current_modbus_stopbits = 0;      //0=1stopbit, 1=2, 2=1.5
  evabus_measurement_client_packet.current_modbus_parity_select = 0; //0=evenparity,1=uneven,2=none

  //set some times
  time = millis();
  stup_timeout = time + 33000; //used to track the startup time
  mb_checktime = time + 33000; //used to track the MB at regular intervals
  ma_checktime = time + 33000; //used to track the mA output at regular intervals

  delay(10);
  watchdog_reset();

}

void loop()
{
  bool no_more = false;
  watchdog_reset();
  // Buttonpress handling:
  debouncer.update();
  if (debouncer.fell()) {
    // on startup the 12V will be left off. If the button is pressed before stup_timeout (~33sec),
    // the effect will be that the 12V will turn on. If the 12V is already on, the button will act as reset again.
    if( millis() < stup_timeout & !target_powered ){
      set_button_color(0, 0, 4);
      set12Vtarget(true);
    } else {
      set_button_color(255, 0, 0);
      set12Vtarget(false);
      delay(200);
      while (1)
        ;   //wait for the WDT
    }
  }
  set_button_color( 16, 0, 0);
  // PI/Host Serial handling
  if (Serial1.available() >= 6) {
    //set_button_color(255, 0, 0); delay(500);
    if (Serial1.read() == 0xab )
    { // Sync byte 1
      //set_button_color(0, 255, 0); delay(500);
      if (Serial1.read() == 0xcd)
      { // Sync byte 2
        //set_button_color(0, 0, 255);  delay(500);
        //sync bytes ok, all messages start with address, length and type:
        evabus_host_packet.address = Serial1.read();
        evabus_host_packet.length = Serial1.read();
        evabus_host_packet.type = Serial1.read();

        // Check if packet is for us
        if (evabus_host_packet.address != EVABUS_SLAVE_ID)
          return;
        //set_button_color(0, 0, 255);  delay(500);
        // read packet data
        if (evabus_host_packet.length > 5)
        {
          Serial1.readBytes(&evabus_host_packet.data[5], evabus_host_packet.length - 5);
          // Check crc
          uint16_t packet_crc = (evabus_host_packet.data[evabus_host_packet.length - 1] << 8) | evabus_host_packet.data[evabus_host_packet.length - 2];
          uint16_t crc = calculateCRC(evabus_host_packet.data, evabus_host_packet.length - 2);
          if (crc == packet_crc)
          {
            no_more = true;
            switch (evabus_host_packet.type)
            {
            case EVABUS_MEASUREMENT_TYPE:
              handle_host_measurement_packet((evabus_host_measurement_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_ID_TYPE:
              handle_host_id_packet((evabus_host_id_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_CMD_TYPE:
              handle_host_cmd_packet((evabus_host_cmd_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_SET_SERIAL_TYPE:
              handle_host_set_serial_packet((evabus_host_set_serial_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_SLAVE_TYPE:
              handle_host_set_slave_packet((evabus_host_set_slave_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_STARTUP_TYPE:
              handle_host_startup_packet((evabus_host_startup_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_SET_SERVOS_TYPE:
              handle_host_set_servos_packet((evabus_host_set_servos_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_READ_MODBUS_TYPE:
              handle_host_read_modbus_packet((evabus_host_read_modbus_packet_t *)&evabus_host_packet);
              break;
            case EVABUS_WRITE_MODBUS_TYPE:
              handle_host_write_modbus_packet((evabus_host_write_modbus_packet_t *)&evabus_host_packet);
              break;
            default:
              blink(10, 100, 255, 0, 0);
              break;
            }
          } // CRC ok
        }   // Packet length
      }     // Sync
    }       // Sync 1
  }  
  set_button_color( 0, 16, 0);
  // Target serial uart handling only during startup
  if( !no_more && target_powered && (millis() < mb_checktime)) {
    while (Serial3.available()) {
      no_more = true;
      set_button_color( 4, 0, 0 );
      buffer+=Serial3.readStringUntil('\n')+String('\n');
    } 
    if( debug_on ){
      if( buffer.length() > 1 ){
        Serial1.println( buffer );
        Serial1.flush();
      }
    }
  }
  while( buffer.indexOf( '\n') >= 0 ){
    set_button_color( 32, 0, 0 );
    if( buffer.indexOf( '\n' ) >= 0 ){
      String buf = buffer.substring( 0, buffer.indexOf( '\n' ) );
     
      checkDebugMessages( buf, &evabus_measurement_client_packet );
      if (millis() < stup_timeout){ //CHECK the startup messages
        stup_timeout = checkStartupMessages( stup_timeout, buf, ok_str, success_str, &evabus_measurement_client_packet );
      }
    }
    if( buffer.indexOf('\n') <= buffer.length() - 1 ){
      buffer = buffer.substring( buffer.indexOf('\n') + 1 );
    } else {
      buffer = "";
      set_button_color(0,0,0);
      break;
    }
  }

  // Serial4 handling is disabled
 

  // Modbus input register handling only after startup
  if( !no_more && MB_check_on && target_powered && (millis() > mb_checktime)) {
    set_button_color(0, 32, 0);
    //delay(5);
    mb_checktime = checkModbusInputRegisters( &evabus_measurement_client_packet, debug_on );
    set_button_color(0, 0, 0);
  }
  set_button_color( 0, 0, 16);
  // 4-20mA readout + PWR_GND
  if( !no_more && mA_check_on && target_powered ){
    //this works for simple, single ended analog input readout
    //differential mode did not work because the negeative input is on AIN10, while the ADC can only handle AIN[0-7] for negative inputs
    voltage1 = ((float)analogRead(MA_CHECK_P) / 1024.0 * AREFVOLT );
    voltage2 = ((float)analogRead(MA_CHECK_N) / 1024.0 * AREFVOLT );
    voltage_diff = voltage1 - voltage2;
    if( (mA < 3.50) | (mA > 20.50 ) ){
      mA = ( voltage_diff * 1000.0 / AMC1301_SHUNT / AMC1301_GAIN ); //no stabilization when out-of-range
    } else {
      mA = 0.98*mA + 0.02*( voltage_diff * 1000.0 / AMC1301_SHUNT / AMC1301_GAIN );  //Some stabilization of readout...
    }
    evabus_measurement_client_packet.MA = mA;
    pwr_gnd_mA = ((float)analogRead(GND_TARGET)) / 1.0240 * AREFVOLT / 1.0;
    if( pwr_gnd_mA > GND_PWR_MAX_MA ){
      digitalWrite(PIN_12V_TGT, LOW);
      blink( (int) (pwr_gnd_mA/100.0), 200, 255, 0, 0);
    }
    evabus_measurement_client_packet.PWR_GND = evabus_measurement_client_packet.PWR_GND*0.98 + 0.02*pwr_gnd_mA;
    if( millis() > ma_checktime ){
      if( debug_on ){
        Serial1.print( "Voltages: " + String( voltage1,3 ) + " , " + String( voltage2, 3 ) + " , " + String( voltage_diff, 3 ) ); //() + "\r\n" ); //Until( '\r' ) );
        Serial1.println( " mA: " + String( voltage_diff * 1000.0 / AMC1301_SHUNT / AMC1301_GAIN ) + ", avg: " + String(mA) );
        Serial1.flush();
      }
      ma_checktime = millis() + 500;
    }
  }

  // Detect target device (radar) by the voltage(s)
  //checkRadarPresence( stup_timeout, target_powered );

  delayMicroseconds(1);
}

void set_servo_angle2( uint8_t servo_id, uint8_t new_angle ){
  Servo servoX;
  switch( servo_id ){
    case 1:
      servoX = servo1;
      break;
    case 2:
      servoX = servo2;
      break;
    case 3:
      servoX = servo3;
      break;
    case 4:
      servoX = servo4;
      break;
    case 5:
      servoX = servo5;
      break;
    default:
      blink(10, 100, 255, 0, 0);
      return;
  }
  //old angle comes from global array
  uint8_t old_angle = current_servo_angles[servo_id-1];
  //translate angle to us
  float a = ( (float) (MAX_PULSE_WIDTH - MIN_PULSE_WIDTH) )/180.0;
  //slow down the movement. it's a bit jerky but large steps are not working at all.
  if( new_angle <= old_angle ){
    for( float j=old_angle;j>=new_angle;j-=0.1){
      //down
      servoX.writeMicroseconds( (int) ( MIN_PULSE_WIDTH + j*a ) ); 
      delay(10);
      watchdog_reset();
    }
  } else {
    //up
    for(  float j=old_angle;j<=new_angle;j+=0.2){
      servoX.writeMicroseconds( (int) ( MIN_PULSE_WIDTH + j*a ) ); 
      delay(10);
      watchdog_reset();
    }
  }
  current_servo_angles[servo_id-1] = new_angle;
}

void handle_host_id_packet(evabus_host_id_packet_t *evabus_host_id_packet)
{
  evabus_client_id_packet.crc = calculateCRC(evabus_client_id_packet.data, evabus_client_id_packet.length - 2);
  Serial1.write(evabus_client_id_packet.data, evabus_client_id_packet.length);
  Serial1.flush();
  set_button_color(128, 128, 128);
  delay(100);
  set_button_color(0,0,0);
}

void handle_host_cmd_packet(evabus_host_cmd_packet_t *evabus_host_cmd_packet)
{
  uint8_t pwrset = evabus_host_cmd_packet->target_12V;
  set_button_color(0, 0, pwrset);
  //acknowledge with an empty packet, just as the bootloader handler does..
  evabus_client_cmd_packet.crc = calculateCRC(evabus_client_cmd_packet.data, evabus_client_cmd_packet.length - 2);
  Serial1.write(evabus_client_cmd_packet.data, evabus_client_cmd_packet.length);
  Serial1.flush();

  if (pwrset > 0)
  {
    set12Vtarget(true);
  }
  else
  {
    set12Vtarget(false);
  }
  delay(100); //little delay to enable the detection of the radar...
}

void handle_host_startup_packet(evabus_host_startup_packet_t *evabus_host_startup_packet)
{
  //copy settings to local structure
  strncpy(startup_cfg.ok_token, &evabus_host_startup_packet->ok_token[0], 8);
  startup_cfg.ok_token[7] = '\0'; //ensure null termination
  strncpy(startup_cfg.success_token, &evabus_host_startup_packet->success_token[0], 8);
  startup_cfg.success_token[7] = '\0';
  startup_cfg.num_ok = evabus_host_startup_packet->num_ok;
  startup_cfg.timeout = evabus_host_startup_packet->timeout;
  //strings
  ok_str = String(startup_cfg.ok_token);
  ok_str.trim();
  ok_str.toLowerCase();
  success_str = String(startup_cfg.success_token);
  success_str.trim();
  success_str.toLowerCase();
  //set the timout for receiving ok's and successes.is also done in set12VTarget
  //stup_timeout = millis() + startup_cfg.timeout*1000;
  //reset counters is done in set12VTarget
  //acknowledge with an empty packet, just as the bootloader handler does..
  evabus_client_startup_packet.crc = calculateCRC(evabus_client_startup_packet.data, evabus_client_startup_packet.length - 2);
  Serial1.write(evabus_client_startup_packet.data, evabus_client_startup_packet.length);
  Serial1.flush();
 
  //power cycle target
  set12Vtarget( false );
  delay(200);
  set12Vtarget( true );
}

void handle_host_measurement_packet(evabus_host_measurement_packet_t *evabus_host_measurement_packet)
{
  uint8_t red, green, blue;
  //control
  red = evabus_host_measurement_packet->red;
  green = evabus_host_measurement_packet->green;
  blue = evabus_host_measurement_packet->blue;
  //uint8_t target_powered_set = evabus_host_measurement_packet->target_12V;

  //apply
  set_button_color(red, green, blue);
  
  //measurement
  evabus_measurement_client_packet.button_state = digitalRead(BUTTON_PIN);
  evabus_measurement_client_packet.target_12V = target_powered;
  evabus_measurement_client_packet.MB_12V  = ((float)analogRead(MB_12V_CHECK))  / 1024.0 * AREFVOLT * (22.0 + 1) / 1.0;
  evabus_measurement_client_packet.MB_GND  = ((float)analogRead(MB_GND_CHECK))  / 1024.0 * AREFVOLT;
  evabus_measurement_client_packet.MB_A    = ((float)analogRead(MB_A_CHECK))    / 1024.0 * AREFVOLT * (22.0 + 1) / 1.0;
  evabus_measurement_client_packet.MB_B    = ((float)analogRead(MB_B_CHECK))    / 1024.0 * AREFVOLT * (22.0 + 1) / 1.0;
  evabus_measurement_client_packet.PWR_5V0 = ((float)analogRead(PWR_5V_CHECK))  / 1024.0 * AREFVOLT * (10.0 + 1) / 1.0;
  evabus_measurement_client_packet.PWR_3V3 = ((float)analogRead(PWR_3V3_CHECK)) / 1024.0 * AREFVOLT * (10.0 + 1) / 1.0;
  evabus_measurement_client_packet.PWR_1V8 = ((float)analogRead(PWR_1V8_CHECK)) / 1024.0 * AREFVOLT * (10.0 + 1) / 1.0; 
  //evabus_measurement_client_packet.PWR_GND = ((float)analogRead(GND_TARGET)) ;   // / 1.0240 * AREFVOLT / 1.5;
  //evabus_measurement_client_packet.MA = ((float)analogRead(MA_CHECK) * 1.00001); //4-20 mA
  //startup info
  //evabus_measurement_client_packet.startup_ok = success_ctr;
  evabus_measurement_client_packet.startup_done = millis() > stup_timeout ? 1 : 0;
  //evabus_measurement_client_packet.startup_num_ok = ok_ctr;
  //crc
  evabus_measurement_client_packet.crc = calculateCRC(evabus_measurement_client_packet.data, evabus_measurement_client_packet.length - 2);
  //send
  Serial1.write(evabus_measurement_client_packet.data, evabus_measurement_client_packet.length);
  Serial1.flush();
  delay(10);
}

void handle_host_set_serial_packet(evabus_host_set_serial_packet_t *evabus_host_set_serial_packet)
{
  //answer host
  Serial1.write(evabus_client_set_serial_packet.data, evabus_client_set_serial_packet.length);
  Serial1.flush();
  //turn off target radar, set baud to another value, restart target, restart Serials
 
  watchdog_reset();
  uint8_t result = node.writeSingleCoil(MB_RADAR_COIL_ADDR, 0);
  if (result == node.ku8MBSuccess)
  {
    set_button_color(0, 32, 0);
    delay(300);
    watchdog_reset();
    node.setTransmitBuffer( 0, evabus_host_set_serial_packet->baud_select );
    node.setTransmitBuffer( 1, evabus_host_set_serial_packet->stopbits );
    node.setTransmitBuffer( 2, evabus_host_set_serial_packet->parity_select );
    result = node.writeMultipleRegisters( MB_MODBUS_BAUD_ID, 3); 
    if (result == node.ku8MBSuccess)
    {
      set_button_color(0, 64, 0);
      Serial5_reconfigure(evabus_host_set_serial_packet->baud_select, evabus_host_set_serial_packet->stopbits, evabus_host_set_serial_packet->parity_select);
      delay(300);
      watchdog_reset();
     
      set_button_color(0, 128, 0);
      delay(300);
    }
    else
    {
      Serial5_reconfigure(evabus_host_set_serial_packet->baud_select, evabus_host_set_serial_packet->stopbits, evabus_host_set_serial_packet->parity_select);
      blink(10, 100, 255, 255, 0);
    }
  }
  else
  {
    //still put serial on the different setting so that the command can be used to change the serial baud polling setting
    Serial5_reconfigure(evabus_host_set_serial_packet->baud_select, evabus_host_set_serial_packet->stopbits, evabus_host_set_serial_packet->parity_select);
    blink(10, 100, 255, 0, 0);
  }
  mb_checktime = millis() + 1000;
}

void handle_host_set_slave_packet(evabus_host_set_slave_packet_t *evabus_host_set_slave_packet)
{
  //stop radar, set slave address, power off, restart modbusmaster, power on
  Serial1.write(evabus_client_set_slave_packet.data, evabus_client_set_slave_packet.length);
  Serial1.flush();
  uint8_t result = node.writeSingleCoil(MB_RADAR_COIL_ADDR, 0);
  set_button_color(0, 16, 0);
  delay(500);
  watchdog_reset();
  if (result == node.ku8MBSuccess)
  {
    result = node.writeSingleRegister(MB_MODBUS_SLAVE_ID, evabus_host_set_slave_packet->slave_address); //.readInputRegisters(0x0000, 4 );
    set_button_color(0, 32, 0);
    delay(500);
    if (result == node.ku8MBSuccess)
    {
      set_button_color(0, 64, 0);
      delay(500);
      set_button_color(0, 0, 0);
      watchdog_reset();
      delay(500);
      node.begin(evabus_host_set_slave_packet->slave_address, Serial5);
      set_button_color(0, 128, 0);
      delay(500);
    }
    else
    {
      node.begin(evabus_host_set_slave_packet->slave_address, Serial5);
      blink(10, 100, 255, 255, 0);
    }
  }
  else
  {
    //reset ModbusMaster node anyway such that the command can be used to change polling address even if write to radar failed.
    node.begin(evabus_host_set_slave_packet->slave_address, Serial5);
    blink(10, 100, 255, 0, 0);
  }
  evabus_measurement_client_packet.current_modbus_slave_id = evabus_host_set_slave_packet->slave_address;
  reset_measurement_values();
}

void handle_host_set_servos_packet(evabus_host_set_servos_packet_t *evabus_host_set_servos_packet)
{
  //answer
  Serial1.write(evabus_client_set_servos_packet.data, evabus_client_set_servos_packet.length);
  Serial1.flush();
  set_button_color( 0, 255, 0);
  if( !servos_attached ){
    //servo
    servo1.attach(SERVO1);
    servo2.attach(SERVO2);
    servo3.attach(SERVO3);
    servo4.attach(SERVO4);
    servo5.attach(SERVO5);
    for( int i=1;i<=5;i++){
      set_servo_angle2( i, MIN_SERVO_ANGLE );  //warning: this will go in 1 step!!
    }
    servos_attached = true;
  }
  set_servo_angle2( evabus_host_set_servos_packet->servo, evabus_host_set_servos_packet->angle );
  set_button_color( 0, 0, 0);
}

void handle_host_read_modbus_packet(evabus_host_read_modbus_packet_t *evabus_host_read_modbus_packet){
  //answer with id
  evabus_client_read_modbus_packet.crc = calculateCRC(evabus_client_read_modbus_packet.data, evabus_client_read_modbus_packet.length - 2);
  Serial1.write(evabus_client_read_modbus_packet.data, evabus_client_read_modbus_packet.length);
  Serial1.flush();
  switch( evabus_host_read_modbus_packet->on_off_single ){
    case 0:
      // turn on at certain interval
      set_button_color( 0, 100, 0);
      mb_checktime = millis() + 3.6e6;
      MB_check_on = true;
      break;
    case 1:
      // turn off
      set_button_color( 100, 0, 0);
      mb_checktime = 0;
      MB_check_on = false;
      break;
    case 2:
      // single measurement
      set_button_color( 0, 0, 100);
      mb_checktime = 0;
      MB_check_on = false;
      (void) checkModbusInputRegisters(  &evabus_measurement_client_packet, debug_on );
      break;
    default:
      blink(10, 100, 255, 0, 0);
      break;
  }
  delay(100);
  set_button_color( 0, 0, 0 );

}

void handle_host_write_modbus_packet(evabus_host_write_modbus_packet_t *evabus_host_write_modbus_packet){
  //answer with id
  evabus_client_write_modbus_packet.crc = calculateCRC(evabus_client_write_modbus_packet.data, evabus_client_write_modbus_packet.length - 2);
  Serial1.write(evabus_client_write_modbus_packet.data, evabus_client_write_modbus_packet.length);
  Serial1.flush();
  //node.setTransmitBuffer( 0, lowWord(  evabus_host_write_modbus_packet->value ) )
  //above: for writing multiple values in 1 go

  set_button_color( 0, 16, 0 );
  node.writeSingleRegister( 
    evabus_host_write_modbus_packet->reg, 
    evabus_host_write_modbus_packet->value
  );
  delay(100);
  set_button_color( 0, 0, 0 );

}

void set_button_color(uint8_t r, uint8_t g, uint8_t b)
{
  analogWrite(LED_RED, r);
  analogWrite(LED_GREEN, g);
  analogWrite(LED_BLUE, b);
}

void blink(int num, int ms, uint8_t r, uint8_t g, uint8_t b)
{
  for (int i = 0; i < num; i++)
  {
    set_button_color(r, g, b);
    delay(ms);
    watchdog_reset();
    set_button_color(0, 0, 0);
    delay(ms);
    watchdog_reset();
  }
}

void debug_16bit(uint16_t val)
{
  for (int c = 0; c < 16; c++)
  {
    if (((val << c) & 0x8000) == 0x8000)
    {
      //strip.setPixelColor(c, strip.Color(255, 255, 255)); // Set pixel 'c' to value 'color'
    }
    else
    {
      //strip.setPixelColor(c, strip.Color(0, 0, 0)); // Set pixel 'c' to value 'color'
    }
  }
  //strip.show();
  delay(5000);
}

uint16_t calculateCRC(uint8_t *buffer, int length)
{
  int i, j;
  uint16_t crc = 0xFFFF;
  uint16_t tmp;

  // calculate crc
  for (i = 0; i < length; i++)
  {
    crc = crc ^ buffer[i];

    for (j = 0; j < 8; j++)
    {
      tmp = crc & 0x0001;
      crc = crc >> 1;
      if (tmp)
      {
        crc = crc ^ 0xA001;
      }
    }
  }

  return crc;
}

uint32_t convert_to_le_float(float val)
{
  uint8_t *bytes = (uint8_t *)&val;

  uint32_t ret = (*(bytes + 1)) | (*(bytes + 0) << 8) | (*(bytes + 3) << 16) | (*(bytes + 2) << 24);
  return ret;
}

void set12Vtarget(bool onoff)
{
  if (onoff) {
    //if( target_powered == 0 ){
    digitalWrite(PIN_12V_TGT, HIGH);
    //with no radar:      80 us delay for < 100 mA; 10 us delay for < 200 mA
    //with radar on jig:  20 us delay for < 200 mA; 10 us delay for < 600 mA
    delayMicroseconds(20);  
    bool ok = true;
    
    Serial3.begin(57600);
    if( ok ){
      target_powered = 1;
      evabus_measurement_client_packet.startup_success = 0;
      evabus_measurement_client_packet.startup_num_ok  = 0;
      stup_timeout = millis() + startup_cfg.timeout * 1000; //check the startup strings until stup_timeout
      mb_checktime = stup_timeout + 37;                     //start polling modbus after stup_timeout
      delay(100);
      digitalWrite(PIN_12V_MA, HIGH);
    }
  } else {
    digitalWrite(PIN_12V_MA, LOW);
    delay(100);
    digitalWrite(PIN_12V_TGT, LOW);
    Serial3.end();
    target_powered = 0;
  }
  reset_measurement_values();
}

void reset_measurement_values(void){
  evabus_measurement_client_packet.button_state = digitalRead(BUTTON_PIN);
  evabus_measurement_client_packet.target_12V = target_powered;
  evabus_measurement_client_packet.MB_12V = 0;
  evabus_measurement_client_packet.MB_GND = 3.3;
  evabus_measurement_client_packet.MB_A = 0;
  evabus_measurement_client_packet.MB_B = 0;
  evabus_measurement_client_packet.PWR_5V0 = 0;
  evabus_measurement_client_packet.PWR_3V3 = 0;
  evabus_measurement_client_packet.PWR_1V8 = 0;
  evabus_measurement_client_packet.PWR_GND = 0;
  evabus_measurement_client_packet.MA = 0;
  //startup info
  //evabus_measurement_client_packet.startup_ok     = success_ctr;
  //evabus_measurement_client_packet.startup_done   = millis() > stup_timeout ? 1 : 0;
  //evabus_measurement_client_packet.startup_num_ok = ok_ctr;
  evabus_measurement_client_packet.modbus_acc_version = 0;
  evabus_measurement_client_packet.modbus_amplitude = 0;
  evabus_measurement_client_packet.modbus_distance = 0;
  evabus_measurement_client_packet.modbus_version = 0;
  evabus_measurement_client_packet.modbus_fail_ctr = 0;
  evabus_measurement_client_packet.modbus_success_ctr = 0;
  evabus_measurement_client_packet.debug_distance = 0;
  evabus_measurement_client_packet.debug_amplitude = 0;
  evabus_measurement_client_packet.debug_slave_id = 0;
  evabus_measurement_client_packet.debug_baud_select = 0;
  evabus_measurement_client_packet.debug_stopbits = 0;
  evabus_measurement_client_packet.debug_parity_select = 0;
}

void Serial5_reconfigure(uint16_t baud_set, uint8_t stopbits, uint8_t par_sel){
  Serial5.end();
  delay(50);
  uint16_t config = 
    ( 
      HARDSER_DATA_8 |
      ( stopbits == 2 ? HARDSER_STOP_BIT_1_5 : (stopbits == 1 ? HARDSER_STOP_BIT_2 : HARDSER_STOP_BIT_1  ) )|
      ( par_sel  == 2 ? HARDSER_PARITY_NONE  : (par_sel  == 1 ? HARDSER_PARITY_ODD : HARDSER_PARITY_EVEN ) ) 
    );
  Serial5.begin( (baud_set == 0 ? 19200 : 9600), config );
  node.begin(1, Serial5);
  delay(50);
  evabus_measurement_client_packet.current_modbus_baud          = (baud_set == 0 ? 19200 : 9600);
  evabus_measurement_client_packet.current_modbus_parity_select = par_sel;
  evabus_measurement_client_packet.current_modbus_stopbits      = stopbits;
  reset_measurement_values();
}

void checkDebugMessages( String buf, evabus_client_measurement_packet_t * m_pkt ){
  int from, to;
  if (buf.indexOf("[RADAR]") >= 0) { //CHECK radar messages
        from = buf.indexOf("D: ");
        to = buf.indexOf(",", from);
        m_pkt->debug_distance = (uint16_t)(buf.substring(from + 2, to).toInt());
        //evabus_measurement_client_packet.debug_distance = (uint16_t)(buf.substring(from + 2, to).toInt());
        from = buf.indexOf("A: ");
        to = buf.indexOf(",", from);
        m_pkt->debug_amplitude = (uint16_t)(buf.substring(from + 2, to).toInt());
        //evabus_measurement_client_packet.debug_amplitude = (uint16_t)(buf.substring(from + 2, to).toInt());
  }
  if (buf.indexOf("[MB] BAUD: ") >= 0) { //CHECK modbus messages
    from = buf.indexOf(": ");
    to = buf.indexOf(",", from);
    m_pkt->debug_baud_select = (uint16_t)(buf.substring(from + 2, to).toInt());
    //evabus_measurement_client_packet.debug_baud_select = (uint16_t)(buf.substring(from + 2, to).toInt());
  }
  if (buf.indexOf("[MB] SLAVE_ID: ") >= 0) {
    from = buf.indexOf(": ");
    to = buf.indexOf(",", from);
    m_pkt->debug_slave_id = (uint16_t)(buf.substring(from + 2, to).toInt());
    //evabus_measurement_client_packet.debug_slave_id = (uint16_t)(buf.substring(from + 2, to).toInt());
  }
  if (buf.indexOf("[MB] PARI: ") >= 0) {
    from = buf.indexOf(": ");
    to = buf.indexOf(",", from);
    m_pkt->debug_parity_select = (uint16_t)(buf.substring(from + 2, to).toInt());
    //evabus_measurement_client_packet.debug_parity_select = (uint16_t)(buf.substring(from + 2, to).toInt());
  }
  if (buf.indexOf("[MB] STOP: ") >= 0) {
    from = buf.indexOf(": ");
    to = buf.indexOf(",", from);
    m_pkt->debug_stopbits = (uint16_t)(buf.substring(from + 2, to).toInt());
    //evabus_measurement_client_packet.debug_stopbits = (uint16_t)(buf.substring(from + 2, to).toInt());
  }
}

unsigned long checkStartupMessages( unsigned long _stup_timeout, String buf, String _ok_str, String _success_str, evabus_client_measurement_packet_t * m_pkt ){
  if (buf.length() > 8) {
    buf.toLowerCase();
    int index = buf.indexOf(_ok_str);
    if (index >= 0) {
       m_pkt->startup_num_ok++;
    }
    index = buf.indexOf(_success_str);
    if (index >= 0) {
      m_pkt->startup_success++;
      _stup_timeout = millis();
    }
    //buf = "";
  }
  return _stup_timeout;
}

unsigned long checkModbusInputRegisters( evabus_client_measurement_packet_t * m_pkt, bool _debug_on ){
  uint8_t result = node.readInputRegisters(0x0000, 4);
  unsigned long _mb_checktime;
  if (result == node.ku8MBSuccess) {
    m_pkt->modbus_distance    = node.getResponseBuffer(0x00); //DISTANCE 1 AMPLITUDE 2 VERSION 3 ACC_VERSION 4
    m_pkt->modbus_amplitude   = node.getResponseBuffer(0x01);
    m_pkt->modbus_version     = node.getResponseBuffer(0x02);
    m_pkt->modbus_acc_version = node.getResponseBuffer(0x03);
    if (m_pkt->modbus_version >= 110) { //TODO: test actual .modbus_version
      result |= node.readInputRegisters(0x00004, 2);
      if (result == node.ku8MBSuccess) {
        m_pkt->radar_serial_number[0] = node.getResponseBuffer(0x00); //SERIALNUMBER H
        m_pkt->radar_serial_number[1] = node.getResponseBuffer(0x01); //SERIALNUMBER L
      }
    } else {
      m_pkt->radar_serial_number[0] = 0x2B; //node.getResponseBuffer(0x04);       //SERIALNUMBER H
      m_pkt->radar_serial_number[1] = 0xAD; //node.getResponseBuffer(0x05);       //SERIALNUMBER L
    }
    if( _debug_on ){
      Serial1.print("Radar distance: ");
      Serial1.println(m_pkt->modbus_distance);
      Serial1.print("Radar amplitude: ");
      Serial1.println(m_pkt->modbus_amplitude);
      Serial1.print("Radar version: ");
      Serial1.println(m_pkt->modbus_version);
      Serial1.print("Radar acconeer version: ");
      Serial1.println(m_pkt->modbus_acc_version);
      Serial1.println("Radar serial_number: " + String(m_pkt->radar_serial_number[0], HEX) );
      //Serial1.println(m_pkt->radar_serial_number[0], HEX);
      //Serial1.println(evabus_measurement_client_packet.radar_serial_number[0], HEX);
      Serial1.println("Radar serial_number: " + String(m_pkt->radar_serial_number[1], HEX) );
      //Serial1.println(node.getResponseBuffer(0x05));
      //Serial1.println(evabus_measurement_client_packet.radar_serial_number[1], HEX);
      Serial1.flush();
    }
  }
  if (result != node.ku8MBSuccess) {
    if( _debug_on ){ 
      Serial1.println("no ku8MBSuccess: " + String( result ) );
      Serial1.flush();
    }
    Serial5.flush(); //flush also the modbus Serial
    set_button_color(128, 0, 0);
    //delay(500); //blocks for too long
    m_pkt->modbus_fail_ctr++;
    _mb_checktime = millis() + 2673;  //the modbus handling already takes long, so increase the interval a little
  }
  else {
    m_pkt->modbus_success_ctr++;
    _mb_checktime = millis() + 1379;
  }
  return _mb_checktime;
}

void checkRadarPresence( unsigned long _stup_timeout, uint8_t _target_powered ){
  float voltage = ((float)analogRead(PWR_5V_CHECK)) / 1024.0 * AREFVOLT * (10.0 + 1) / 1.0;
  float MB_GND  = ((float)analogRead(MB_GND_CHECK)) / 1024.0 * AREFVOLT;
  if( (voltage > 4.5) | (MB_GND < 0.5 ) ){
    set_button_color(0, 0, 1);
  } else {
    if (millis() > _stup_timeout) {
      set_button_color(4, 0, 0);
      set12Vtarget(0);
    }
  }
}