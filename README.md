###

started with main.h and main.ino
set up platformio with atmega2560
many libraries are missing and not standard in pio -> ask Erik
- many libraries were not necessary, other could be included in the .ini file
upload using avrisp (i updated udev rules etc, etc):
```
pio run -t program
```


###

For SAMD21 another .ini was created, Along with a boards and a variants directory, along with all the files in there.
Especially interesting is the pin mapping in variant.cpp, this needs to be adjusted.
Since all the A0-AXX were scrambled and redefined three times over, it is easier to use the entry in variant.cpp directly.
This uses the atmel ICE programmer.
```
pio run
pio run -t upload
```

### UUID

The UUID is read from modbus. If something goes wrong, the UUID is set to: 0x2BAD.

### HW VERSION 3

- serial to pi changed to 256000 baud 8N1
- cleaned up pin definitions in variant.cpp
- moved other pin definitions to variant.h

check:
  - V LED
  - V Serial1 (Pi) 
  - V Switches
  - V program SAMD11
  - V comms with PI @ 256000
  - V set resistance value of GND_TGT to 1.0 (from 1.5)
  - V RS485 after modbus
  - V debug serial
  - V programming while in JIG
  - V mA readout -> +/- wrong on board
  - 


todo:
  - implement flags of the switches
  - reactivate detect_radar_presence
  - reactivate current monitoring after power_tgt_on



###

 [MODBUS] THRESH: 300
                     [MODBUS] AVG_FACT: 7
                                         [RADAR] D: 156, A: 576 DAC: 829 START: 150, LEN: 1950
                                                                                              [MODBUS] RSTART: 150
                                                                                                                  [MODBUS] REND: 1950
                                                                                                                                     [MODBUS] GAIN: 6
                                                                                                                                                     [MODBUS] THRESH: 300
                                                                                                                                                                         [MODBUS] AVG_FACT: 7
                [RADAR] D: 156, A: 592 DAC: 829 START: 150, LEN: 1950
                                                                     [MODBUS] RSTART: 150
                                                                                         [MODBUS] REND: 1950
                                                                                                            [MODBUS] GAIN: 6
                                                                                                                            [MODBUS] THRESH: 300
                                                                                                                                                [MODBUS] AVG_FACT: 7
                                                                                                                                                                    [RADAR] D: 155, A: 600 DAC: 828 START: 150, LEN: 1950
                                            [MODBUS] RSTART: 150
                                                                [MODBUS] REND: 1950
                                                                                   [MODBUS] GAIN: 6
                                                                                                   [MODBUS] THRESH: 300
                                                                                                                       [MODBUS] AVG_FACT: 7
                                                                                                                                           [RADAR] D: 155, A: 592 DAC: 828 START: 150, LEN: 1950
                   [MODBUS] RSTART: 150
                                       [MODBUS] REND: 1950
                                                          [MODBUS] GAIN: 6
                                                                          [MODBUS] THRESH: 300
                                                                                              [MODBUS] AVG_FACT: 7
                                                                                                                  [RADAR] D: 155, A: 560 DAC: 828 START: 150, LEN: 1950
                                                                                                                                                                       [MODBUS] RSTART: 150
              [MODBUS] REND: 1950
                                 [MODBUS] GAIN: 6
                                                 [MODBUS] THRESH: 300
                                                                     [MODBUS] AVG_FACT: 7
                                                                                         [RADAR] D: 154, A: 560 DAC: 826 START: 150, LEN: 1950


### good startup:

SAMD21 niqo jig reporting for duty
[EEPROM] INIT
[EEPROM] DATA VALID
RADAR LEVEL SENSOR
Version: 100
EEPROM dump:
[00]: 4294967295
[01]: 4294967295
[02]: 4294967295
[03]: 4294967295
[04]: 4294967295
[05]: 4294967295
[06]: 4294967295
[07]: 4294967295
[08]: 4294967295
[09]: 4294967295
[RADAR] INIT
[RADAR] HAL INIT
00:00:00.000 [    0] (I) (rss): Radar system services activated
[MODBUS] SID: 1
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[USART] INIT
[MODBUS] INIT
[MODBUS] APP INIT
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] TASK START
00:00:00.195 [536891704] (I) (core_a111_r2c): Pll status (4)
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
00:00:13.822 [536891704] (I) (core_a111_r2c): Dll slope status (50)
[MODBUS] Input Reg. Addr:0, Size: 4
00:00:14.706 [536891704] (I) (core_a111_r2c): Dll offset status (5, 152)
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] Input Reg. Addr:0, Size: 4
00:00:26.749 [536891704] (I) (core_a111_r2c): Area status (1, 32, 36005)
00:00:26.763 [536891704] (I) (core_a111_r2c): Sensor calibration successful.
[MODBUS] Input Reg. Addr:0, Size: 4
00:00:27.562 [536891704] (I) (core_a111_r2c): Offset_calibration status: 6428
00:00:27.577 [536891704] (I) (sweep_configuration): sensor 1 config: 7 1 9 6 7 9 0 0 D
00:00:27.594 [536891704] (W) (sweep_configuration): Repetition mode set to max frequency so no specific frequency available
Free space absolute offset: 6 mm
Actual start: 150 mm
Actual length: 1950 mm
Actual end: 2100 mm
Data length: 4027
00:00:27.800 [536946112] (I) (session_sweep_and_processing): State null: Activate request
00:00:27.818 [536891704] (I) (session_sweep_and_processing): acc_session_sweep_and_processing_activate: Activation confirmed
acc_service_activate() 0 => Ok
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2504 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2424 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2464 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2464 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2448 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2400 DAC: 822 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 153, A: 2424 DAC: 824 START: 150, LEN: 1950
[MODBUS] Input Reg. Addr:0, Size: 4
[MODBUS] RSTART: 150
[MODBUS] REND: 1950
[MODBUS] GAIN: 6
[MODBUS] THRESH: 300
[MODBUS] AVG_FACT: 7
[RADAR] D: 152, A: 2480 DAC: 822 START: 150, LEN: 1950
[