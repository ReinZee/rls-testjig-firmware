#ifndef Watchdog_h
#define Watchdog_h
#include <Arduino.h>

void watchdog_init( uint8_t wdt_timeout );

void watchdog_reset();

#endif