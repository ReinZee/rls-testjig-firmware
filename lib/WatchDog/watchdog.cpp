#include "watchdog.h"

//lightweigth version of https://github.com/javos65/WDTZero/blob/master/WDTZero/src/WDTZero.cpp
//use the above reference if not working

void watchdog_init( uint8_t wdt_timeout) {

  // Generic clock generator 2, divisor = 32 (2^(DIV+1))  = _x
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(2) | GCLK_GENDIV_DIV(4);
  // Enable clock generator 2 using low-power 32.768kHz oscillator.
  // With /32 divisor above, this yields 1024Hz clock.
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(2) |
                      GCLK_GENCTRL_GENEN |
                      GCLK_GENCTRL_SRC_OSCULP32K |
                      GCLK_GENCTRL_DIVSEL;
  while(GCLK->STATUS.bit.SYNCBUSY);
  // WDT clock = clock gen 2
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_WDT |
                      GCLK_CLKCTRL_CLKEN |
                      GCLK_CLKCTRL_GEN_GCLK2;

  REG_WDT_CONFIG = wdt_timeout;          // Set the WDT reset timeout to 16 seconds
  REG_WDT_CTRL = WDT_CTRL_ENABLE;               // Enable the WDT in normal mode
  while(WDT->STATUS.bit.SYNCBUSY);              // Wait for synchronization
}

void watchdog_reset() {
  WDT->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;         // Clear WTD bit
  while(WDT->STATUS.bit.SYNCBUSY);              // Wait for synchronization
}